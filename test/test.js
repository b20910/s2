const { getCircleArea,checkIfPassed,getAverage,getSum,getDifference,div_check,isOddOrEven,reverseString } = require('../src/util.js');
// import the assert statements from our chai
const { expect,assert } = require('chai');

// test case - a condition we are testing
// it(stringExplainsWhatTheTestDoes,functionToTest)
// assert is used to assert conditions for the test to pass. If the assertion fails then the test is considered failed.
// describe() is used to create a test suite. A test suite is a group of test cases related to one another or tests the same method, data, or functions.

describe('test_get_circle_area',() => {

	it('test_area_of_circle_radius_15_is_706.86',() => {

		let area = getCircleArea(15);
		assert.equal(area,706.86);

	});

	it('test_area_of_circle_radius_300_is_282744',() => {

		let area = getCircleArea(300);
		expect(area).to.equal(282744);

	});

});

describe('test_check_if_passed',() => {

	it('test_25_out_of_30_if_passed',() => {

		let isPassed = checkIfPassed(25,30);
		assert.equal(isPassed,true);

	});

	it('test_30_out_of_50_is_not_passed',() => {

		let isPassed = checkIfPassed(30,50);
		assert.equal(isPassed,false);

	});

});

describe('test_get_average',() => {

	it('test_average_of_80_82_84_86_is_83',() => {

		let average = getAverage(80,82,84,86);
		assert.equal(average,83);

	});

	it('test_average_of_70_80_82_84_is_79',() => {

		let average = getAverage(70,80,82,84);
		assert.equal(average,79);

	});

});

describe('test_get_sum',() => {

	it('test_sum_of_15_30_is_45',() => {

		let sum = getSum(15,30);
		assert.equal(sum,45);

	});

	it('test_sum_of_25_50_is_75',() => {

		let sum = getSum(25,50);
		assert.equal(sum,75);

	});

});

describe('test_get_difference',() => {

	it('test_difference_of_70_40_is_30',() => {

		let diff = getDifference(70,40);
		assert.equal(diff,30);

	});

	it('test_difference_of_125_50_is_75',() => {

		let diff = getDifference(125,50);
		assert.equal(diff,75);

	});

});

describe('test_div_check_by_5_or_7',() => {

	it('test_25_div_by_5_is_true',() => {

		let isDivBy5 = div_check(25);
		assert.equal(isDivBy5,true);

	});

	it('test_46_div_by_5_is_false',() => {

		let isDivBy5 = div_check(46);
		assert.equal(isDivBy5,false);

	});

	it('test_21_div_by_7_is_true',() => {

		let isDivBy7 = div_check(21);
		assert.equal(isDivBy7,true);

	});

	it('test_36_div_by_7_is_false',() => {

		let isDivBy7 = div_check(36);
		assert.equal(isDivBy7,false);

	});

});

describe('test_is_odd_or_even',() => {

	it('test_40_is_even',() => {
		assert.equal(isOddOrEven(40),'even');
	});

	it('test_100_is_even',() => {
		assert.equal(isOddOrEven(100),'even');
	});

	it('test_3_is_odd',() => {
		assert.equal(isOddOrEven(3),'odd');
	});

	it('test_29_is_odd',() => {
		assert.equal(isOddOrEven(29),'odd');
	});

});

describe('test_reverse_string',() => {

	it('test_reverse_of_hello_is_olleh',() => {
		let reversedString = reverseString('hello');
		assert.equal(reversedString,'olleh');
	});

	it('test_reverse_of_supercalifragilisticexpialidocious_is_suoicodilaipxecitsiligarfilacrepus.',() => {
		let reversedString = reverseString('supercalifragilisticexpialidocious');
		assert.equal(reversedString,'suoicodilaipxecitsiligarfilacrepus');
	});

	it('test_reverse_of_good_is_doog',() => {
		let reversedString = reverseString('good');
		assert.equal(reversedString,'doog');
	});

	it('test_reverse_of_blackpink_is_knipkcalb',() => {
		let reversedString = reverseString('blackpink');
		assert.equal(reversedString,'knipkcalb');
	});

});

