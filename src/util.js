// Sample Functions to Test

function getCircleArea(radius){

	// area = pi x r^2

	return 3.1416*(radius**2);

}

function checkIfPassed(score,total){

	// score/total*100 >= 75

	return (score/total)*100 >= 75;

}

function getAverage(num1,num2,num3,num4){

	return (num1+num2+num3+num4)/4;

}

function getSum(num1,num2){

	return (num1+num2);
}

function getDifference(num1,num2){

	return (num1-num2);
}

function div_check(num){

	if(num % 5 === 0 || num % 7 === 0){
		return true;
	};
	return false;
}

function isOddOrEven(num){

	if(num % 2 === 0){
		return 'even';
	};
	return 'odd';
}

function reverseString(str){

	let reversedString = "";
	for(let i = str.length - 1; i >= 0; i--){
		reversedString += str[i];
	}
	return reversedString;
}

module.exports = {
	getCircleArea: getCircleArea,
	checkIfPassed: checkIfPassed,
	getAverage: getAverage,
	getSum: getSum,
	getDifference: getDifference,
	div_check: div_check,
	isOddOrEven: isOddOrEven,
	reverseString: reverseString
}

/*
	Mini-Activity

	Create a new test suite to test cases for the get average function

	test case 1: assert that the average of 80,82,84, and 86 is 83.
	test case 2: assert that the average of 70,80,82 and 84 is 79.
*/

/* 
	Mini-Activity #2

	Create a function called getSum which is able to receive 2 numbers and returns the sum of both arguments.

	Create a test suite to check the function result for the following pairs of numbers:
	15,30
	25,50

	Create a function called getDifference which is able to recieve 2 numbers and returns the difference of both arguments.

	Create a test suite to check the function result for the following pairs of numbers:
	70,40
	125,50
*/

